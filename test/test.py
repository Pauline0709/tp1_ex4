#!/usr/bin/env python
# coding: utf_8

from calculator.simplecalculator import SimpleCalculator as SimpleCalculator

"""
Module de test
"""

if __name__ == "__main__":
	"""
	Test
	"""

	VAL_A = 1
	VAL_B = 2
	MY_CALCULATOR = SimpleCalculator()
	print(MY_CALCULATOR.sum(VAL_A, VAL_B))
	print(MY_CALCULATOR.substract(VAL_A, VAL_B))
	print(MY_CALCULATOR.multiply(VAL_A, VAL_B))
	print(MY_CALCULATOR.divide(VAL_A, VAL_B))
