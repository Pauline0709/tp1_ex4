#!/usr/bin/env python
# coding: utf_8

"""
Module simplecalculator
"""


class SimpleCalculator:
    """ 
	Classe permettant de faire la somme, la soustraction, la multiplication et division
	
	"""

    @staticmethod
    def sum(var_a, var_b):
        """
		Addition de deux nombres
		Entrees: var_a et var_b deux entiers
		Return: un entier correspondant à la somme
		"""
        return var_a + var_b

    @staticmethod
    def substract(var_a, var_b):
        """
		Soustraction de deux nombres
		Entrees: var_a et var_b deux entiers
		Return: un entier correspondant à la soustraction
		"""
        return var_a - var_b

    @staticmethod
    def multiply(var_a, var_b):
        """
		Multiplication de deux nombres
		Entrees: var_a et var_b deux entiers
		Return: un entier correspondant à la multiplication
		"""
        return var_a * var_b

    @staticmethod
    def divide(var_a, var_b):
        """
		Division de deux nombres
		Entrees: var_a et var_b deux entiers
		Return: un entier correspondant à la division
		"""
        return var_a / var_b


if __name__ == "__main__":
    """
	Test 
	"""

VAL_A = 1
VAL_B = 2
MY_CALCULATOR = SimpleCalculator()
print(MY_CALCULATOR.sum(VAL_A, VAL_B))
print(MY_CALCULATOR.substract(VAL_A, VAL_B))
print(MY_CALCULATOR.multiply(VAL_A, VAL_B))
print(MY_CALCULATOR.divide(VAL_A, VAL_B))
